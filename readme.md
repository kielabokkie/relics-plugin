# Relics

## Installation

To add the relics plugin to your project add the following to your `composer.json`:

    {
        "repositories": [
            {
                "type": "vcs",
                "url": "https://bitbucket.org/kielabokkie/relics-plugin.git"
            }
        ],
        "require-dev": {
            "kielabokkie/relics": "0.*",
        },
    }

## Config

Next you have to create a `relics.json` file to the root of your project and copy paste the following config:

    {
    	"api_url": "http://relics.io/api/jobs",
    	"api_token": "{{ YOUR API TOKEN }}",
    	"project_uuid": "{{ YOUR PROJECT UUID }}",
    	"files": {
        	"checkstyle": "build/checkstyle.xml",
        	"phpmd": "build/phpmd.xml"
    	}
	}

Replace the API token with your private API token that you can find on the account page and the project UUID can be found on the projects page on relics.io.

The files array contains the location of your build relics (currently only checkstyle and phpmd are supported).

## Build

The easiest way to have your relics sent to the server is by adding it to your `build.xml` file:

    <?xml version="1.0" encoding="UTF-8"?>
    <project name="relics" default="build">
        <target name="build" depends="phpcs-ci, phpmd-ci, relics"/>

        <target name="phpcs-ci" description="Find coding standard violations using PHP_CodeSniffer creating a log file for the continuous integration server">
            <exec executable="${basedir}/vendor/bin/phpcs" output="/dev/null">
                <arg path="${basedir}/app" />
                <arg value="--report=checkstyle" />
                <arg value="--report-file=${basedir}/build/checkstyle.xml" />
                <arg value="--standard=${basedir}/build/config/ruleset.xml"/>
                <arg value="--ignore=app/database/migrations,app/views"/>
            </exec>
        </target>

        <target name="phpmd-ci" description="Perform project mess detection using PHPMD creating a log file for the continuous integration server">
            <exec executable="${basedir}/vendor/bin/phpmd">
                <arg path="${basedir}/app" />
                <arg value="xml" />
                <arg value="${basedir}/build/phpmd_rules.xml" />
                <arg value="--reportfile" />
                <arg value="${basedir}/build/phpmd.xml" />
                <arg value="--exclude" />
                <arg value="${basedir}/app/database/migrations/*" />
            </exec>
        </target>

        <target name="relics" description="Send build relics to the relics.io server">
            <exec executable="php" failonerror="true">
                <arg value="${basedir}/vendor/kielabokkie/relics/bin/console"/>
                <arg value="relics:dig"/>
            </exec>
        </target>

    </project>
