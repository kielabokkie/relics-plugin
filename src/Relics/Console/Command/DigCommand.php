<?php
namespace Relics\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class DigCommand extends Command {
    protected function configure() {
        $this->setName("relics:dig")
            ->setDescription("Digs up relics and send them to the remote server")
            ->addOption(
                'config-path',
                null,
                InputOption::VALUE_OPTIONAL,
                'The path to the relics.json config file (i.e. bin/console relics:dig --config-path="/var/www/")'
            )
            ->setHelp(<<<EOT
The <info>dig</info> command digs up the relics of the build and sends them to the remote server.
EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $output->writeln(sprintf("\n<info>Digging started...</info>"));

        if ($input->getOption('config-path')) {
            $rootDir = $input->getOption('config-path');
        } else {
            $rootDir = __DIR__.'/../../../../../../../';
        }

        if (!file_exists($rootDir.'relics.json')) {
            $output->writeln(sprintf("\n<error>Error: config file [%s] not found.</error>\n", $rootDir.'relics.json'));
            exit();
        }

        $commmitHash = exec('git rev-parse HEAD');
        $commmitAuthor = exec('git --no-pager log -1 --pretty=format:"%aN" HEAD');
        $commmitAuthorEmail = exec('git --no-pager log -1 --pretty=format:"%aE" HEAD');

        $payload = array(
            'commit_hash'         => $commmitHash,
            'commit_author'       => $commmitAuthor,
            'commit_author_email' => $commmitAuthorEmail,
        );

        $configFile = file_get_contents($rootDir.'relics.json');
        $config = json_decode($configFile, true);

        // Add the files to the payload
        $files = $config['files'];
        foreach ($files as $fileType => $file) {
            $filePath = realpath($rootDir.$file);
            $payload[sprintf('files[%s]', $fileType)] = '@'.$filePath.';type=text/xml';
        }

        // Send payload to the API
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $config['api_url']);
        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array(sprintf('X-API-TOKEN: %s', $config['api_token']), sprintf('X-PROJECT-UUID: %s', $config['project_uuid'])));
        curl_setopt($curlHandle, CURLOPT_POST, 1);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curlHandle);
        curl_close ($curlHandle);

        $json = json_decode($result, true);

        if ($json['success'] === true) {
            $output->writeln(sprintf("\n<info>Successfully dug up %s.</info>\n", count($files) == 1 ? '1 file' : count($files) . ' files'));
        } else {
            $output->writeln("\n<error>Oops, something went wrong. Your files were not sent to the API.</error>\n");

            $formatter = $this->getHelperSet()->get('formatter');
            $formattedBlock = $formatter->formatBlock(print_r($json['errors'], true), 'error');
            $output->writeln($formattedBlock);
        }
    }
}
